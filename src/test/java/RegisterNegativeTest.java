import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import HelperClass.Helper;




public class RegisterNegativeTest {

    public WebDriver driver;

    public String URI = Helper.URI;

    @BeforeMethod
    public void setup()
    {
        System.setProperty("webdriver.gecko.driver", Helper.geckoLocation);

        driver = new FirefoxDriver();

        driver.navigate().to(this.URI);
    }

    @Test
    public void demoOpenCartNegativeRegister() throws InterruptedException
    {
        // klik na my account
        WebElement dropdownMenu = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/a"));
        dropdownMenu.click();

        // klik na register
        WebElement registerButton = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/ul/li[1]/a"));
        registerButton.click();

        // unos imena
        WebElement firstNameTextbox = driver.findElement(By.name("firstname"));
        firstNameTextbox.sendKeys(Helper.firstname);

        // unos prezimena
        WebElement lastNameTextbox = driver.findElement(By.name("lastname"));
        lastNameTextbox.sendKeys(Helper.lastname);

        // unos email adrese
        WebElement emailTextbox = driver.findElement(By.name("email"));
        emailTextbox.sendKeys(Helper.email);

        // unos telefonskog broja
        WebElement telephoneTextbox = driver.findElement(By.name("telephone"));
        telephoneTextbox.sendKeys(Helper.telephone);

        // unos lozinke
        WebElement passwordTextbox = driver.findElement(By.name("password"));
        passwordTextbox.sendKeys(Helper.password);

        // unos ponovljene lozinke
        WebElement repeatPasswordTextbox = driver.findElement(By.name("confirm"));
        repeatPasswordTextbox.sendKeys(Helper.password);

        // odabir primanja informativnih letaka
        WebElement newsletterRadioButton = driver.findElement(By.name("newsletter"));
        newsletterRadioButton.click();

        // prihvaćanje pravila privatnosti
        WebElement privacyPolicyConfirm = driver.findElement(By.name("agree"));
        privacyPolicyConfirm.click();

        // odabir tipke za nastavak postupka registracije
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[2]/div/div/form/div/div/input[2]"));
        continueButton.click();

        // uzimanje teksta s nove stranice kako bi se odredio uspjeh ili neuspjeh testa
        WebElement errorTextbox = driver.findElement(By.xpath("/html/body/div[2]/div[1]"));
        String comparisonText = errorTextbox.getText();

        if(comparisonText.contains("Warning: E-Mail Address is already registered!"))
        {
            System.out.println("2. testni slučaj: uspjeh! Identificiran postojeći korisnički račun, tj. sustav dobro prepoznaje da takav korisnik već postoji u sustavu.");
        }
        else
        {
            System.out.println("2. testni slučaj: neuspjeh! Korisnik se uspio registrirati više od jednog puta te je potrebno zabilježiti grešku i ispraviti je što prije.");
        }


    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }

}
