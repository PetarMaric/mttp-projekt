import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod ;
import org.testng.annotations.BeforeMethod ;
import org.testng.annotations.Test ;
import HelperClass.Helper;



public class LoginTest {

    public WebDriver driver;

    public String URI = Helper.URI;

    @BeforeMethod
    public void setup()
    {
        System.setProperty("webdriver.gecko.driver", Helper.geckoLocation);

        driver = new FirefoxDriver();

        driver.navigate().to(this.URI);
    }

    @Test
    public void demoOpenCartLogin() throws InterruptedException
    {
        // klik na my account
        WebElement dropdownMenu = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/a"));
        dropdownMenu.click();

        // klik na login
        WebElement loginButton = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/ul/li[2]/a"));
        loginButton.click();

        // unos email adrese
        WebElement emailTextbox = driver.findElement(By.name("email"));
        emailTextbox.sendKeys(Helper.email);

        // unos lozinke
        WebElement passwordTextbox = driver.findElement(By.name("password"));
        passwordTextbox.sendKeys(Helper.password);

        // odabir tipke za nastavak postupka prijave
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/div/form/input"));
        continueButton.click();

        // uzimanje teksta s nove stranice kako bi se odredio uspjeh ili neuspjeh testa
        WebElement loginText = driver.findElement(By.xpath("/html/body/div[2]/div/div/ul[1]/li[1]/a"));
        String text = loginText.getText();

        if(text.contains("Edit your account information"))
        {
            System.out.println("3. testni slučaj: uspjeh! Uspješan login.");
        }
        else
        {
            System.out.println("3. testni slučaj: neuspjeh! Login nije moguć zato što ne postoji korisnički račun. Probati kreirati korisnički račun.");
        }

    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }

}
