import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import HelperClass.Helper;

public class RemovingProductFromComparisonTest {

    public WebDriver driver;

    public String URI = Helper.URI;

    @BeforeMethod
    public void setup()
    {
        System.setProperty("webdriver.gecko.driver", Helper.geckoLocation);

        driver = new FirefoxDriver();

        driver.navigate().to(this.URI);
    }

    @Test
    public void demoOpenCartRemovingFromComparison() throws InterruptedException
    {
        // klik na my account
        WebElement dropdownDesktopMenu = driver.findElement(By.xpath("/html/body/div[1]/nav/div[2]/ul/li[1]/a"));
        dropdownDesktopMenu.click();

        // odabir maca iz dropdown menija
        WebElement macDesktopSelection = driver.findElement(By.xpath("/html/body/div[1]/nav/div[2]/ul/li[1]/div/div/ul/li[2]/a"));
        macDesktopSelection.click();

        // odabir imaca za usporedbu uređaja
        WebElement iMacComparisonSelection = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div/div/div[2]/div[2]/button[3]"));
        iMacComparisonSelection.click();
        Thread.sleep(500);

        // upis macbook pro u tražilicu
        WebElement macbookproInput = driver.findElement(By.xpath("/html/body/header/div/div/div[2]/div/input"));
        macbookproInput.sendKeys("macbook pro");

        // traženje laptopa
        WebElement searchConfirmation = driver.findElement(By.xpath("/html/body/header/div/div/div[2]/div/span/button"));
        searchConfirmation.click();

        // dodavanje macbook pro laptopa za usporedbu uređaja
        WebElement macbookproComparisonSelection = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/div/div[2]/div[2]/button[3]"));
        macbookproComparisonSelection.click();
        Thread.sleep(500);

        // odabir opcije za odlazak na ekran usporedbe
        WebElement productComparisonNavigation = driver.findElement(By.xpath("//*[@id=\"compare-total\"]"));
        productComparisonNavigation.click();

        // uklanjanje jednog od uređaja iz usporedbe
        WebElement removingProduct = driver.findElement(By.xpath("/html/body/div[2]/div/div/table/tbody[2]/tr/td[2]/a"));
        removingProduct.click();

        // uzimanje teksta s nove stranice kako bi se odredio uspjeh ili neuspjeh testa
        WebElement productSelectionText = driver.findElement(By.xpath("/html/body/div[2]/div[1]"));
        String text = productSelectionText.getText();

        if(text.contains("Success: You have modified your product comparison!"))
        {
            System.out.println("7. testni slučaj: uspjeh! Uklonjen je jedan od uređaja.");
        }
        else
        {
            System.out.println("7. testni slučaj: neuspjeh! Negdje je došlo do pogreške.");
        }

    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }

}
