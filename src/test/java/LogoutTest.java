import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import HelperClass.Helper;



public class LogoutTest {

    public WebDriver driver;

    public String URI = Helper.URI;

    @BeforeMethod
    public void setup()
    {
        System.setProperty("webdriver.gecko.driver", Helper.geckoLocation);

        driver = new FirefoxDriver();

        driver.navigate().to(this.URI);
    }

    @Test
    public void demoOpenCartLogout() throws InterruptedException
    {
        // klik na my account
        WebElement dropdownMenu = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/a"));
        dropdownMenu.click();

        // klik na login
        WebElement loginButton = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/ul/li[2]/a"));
        loginButton.click();

        // unos email adrese
        WebElement emailTextbox = driver.findElement(By.name("email"));
        emailTextbox.sendKeys(Helper.email);

        // unos lozinke
        WebElement passwordTextbox = driver.findElement(By.name("password"));
        passwordTextbox.sendKeys(Helper.password);

        // odabir tipke za nastavak postupka odlogiravanja
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/div/form/input"));
        continueButton.click();
        Thread.sleep(1000);

        // ponovni odabir padajućeg menija my account
        WebElement newDropdownMenu = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/a"));
        newDropdownMenu.click();

        // odabir tipke logout
        WebElement logoutButton = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/ul/li[5]/a"));
        logoutButton.click();

        // uzimanje teksta s nove stranice kako bi se odredio uspjeh ili neuspjeh testa
        WebElement logoutText = driver.findElement(By.xpath("/html/body/div[2]/div/div/h1"));
        String text = logoutText.getText();

        if(text.contains("Account Logout"))
        {
            System.out.println("5. testni slučaj: uspjeh! Uspješan logout.");
        }
        else
        {
            System.out.println("5. testni slučaj: neuspjeh! Logout neuspješan.");
        }

    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }

}
