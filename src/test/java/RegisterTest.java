import HelperClass.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



public class RegisterTest {

    public WebDriver driver;

    public String URI = Helper.URI;

    @BeforeMethod
    public void setup()
    {
        System.setProperty("webdriver.gecko.driver", Helper.geckoLocation);

        driver = new FirefoxDriver();

        driver.navigate().to(this.URI);
    }

    @Test
    public void demoOpenCartRegister() throws InterruptedException
    {
        // klik na my account
        WebElement dropdownMenu = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/a"));
        dropdownMenu.click();

        // klik na register
        WebElement registerButton = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/ul/li[1]/a"));
        registerButton.click();

        // unos imena
        WebElement firstNameTextbox = driver.findElement(By.name("firstname"));
        firstNameTextbox.sendKeys(Helper.firstname);

        // unos prezimena
        WebElement lastNameTextbox = driver.findElement(By.name("lastname"));
        lastNameTextbox.sendKeys(Helper.lastname);

        // unos email adrese
        WebElement emailTextbox = driver.findElement(By.name("email"));
        emailTextbox.sendKeys(Helper.email);

        // unos telefonskog broja
        WebElement telephoneTextbox = driver.findElement(By.name("telephone"));
        telephoneTextbox.sendKeys(Helper.telephone);

        // unos lozinke
        WebElement passwordTextbox = driver.findElement(By.name("password"));
        passwordTextbox.sendKeys(Helper.password);

        // unos ponovljene lozinke
        WebElement repeatPasswordTextbox = driver.findElement(By.name("confirm"));
        repeatPasswordTextbox.sendKeys(Helper.password);

        // odabir primanja informativnih letaka
        WebElement newsletterRadioButton = driver.findElement(By.name("newsletter"));
        newsletterRadioButton.click();

        // prihvaćanje pravila privatnosti
        WebElement privacyPolicyConfirm = driver.findElement(By.name("agree"));
        privacyPolicyConfirm.click();

        // odabir tipke za nastavak postupka registracije
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[2]/div/div/form/div/div/input[2]"));
        continueButton.click();

        // uzimanje teksta s nove stranice kako bi se odredio uspjeh ili neuspjeh testa
        WebElement accountCreationText = driver.findElement(By.xpath("/html/body/div[2]/div/div/h1"));
        String text = accountCreationText.getText();

        if(text.contains("Your Account Has Been Created!"))
        {
            System.out.println("1. testni slučaj: uspjeh! Kreiran korisnički račun.");
        }
        else
        {
            System.out.println("1. testni slučaj: neuspjeh! Nije se mogao kreirati korisnički račun zato što već postoji taj korisnik. Probati promijeniti email i ponovno pokrenuti test.");
        }

    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }

}
