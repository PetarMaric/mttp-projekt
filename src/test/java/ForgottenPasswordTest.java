import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import HelperClass.Helper;



public class ForgottenPasswordTest {

    public WebDriver driver;

    public String URI = Helper.URI;

    @BeforeMethod
    public void setup()
    {
        System.setProperty("webdriver.gecko.driver", Helper.geckoLocation);

        driver = new FirefoxDriver();

        driver.navigate().to(this.URI);
    }

    @Test
    public void demoOpenCartForgottenPassword() throws InterruptedException
    {
        // klik na my account
        WebElement dropdownMenu = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/a"));
        dropdownMenu.click();

        // klik na login
        WebElement loginButton = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/ul/li[2]/a"));
        loginButton.click();

        // odabir poveznice koja preusmjerava na novu stranicu za unos email adrese
        WebElement forgottenPassword = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/div/form/div[2]/a"));
        forgottenPassword.click();

        // unos email adrese za kreiranje nove lozinke
        WebElement emailTextbox = driver.findElement(By.name("email"));
        emailTextbox.sendKeys(Helper.email);

        // nastavak postupka kreiranja nove lozinke
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[2]/div/div/form/div/div[2]/input"));
        continueButton.click();

        // uzimanje teksta s nove stranice kako bi se odredio uspjeh ili neuspjeh testa
        WebElement confirmationText = driver.findElement(By.xpath("/html/body/div[2]/div[1]"));
        String text = confirmationText.getText();
        Thread.sleep(1000);

        if(text.contains("An email with a confirmation link has been sent your email address."))
        {
            System.out.println("4. testni slučaj: uspjeh! Uspješno poslan mail za resetiranje korisničke lozinke.");
        }
        else
        {
            System.out.println("4. testni slučaj: neuspjeh! Nemoguće poslati mail na email adresu koja ne postoji.");
        }

    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }

}
