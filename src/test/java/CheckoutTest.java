import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import HelperClass.Helper;



public class CheckoutTest {

    public WebDriver driver;

    public String URI = Helper.URI;

    @BeforeMethod
    public void setup()
    {
        System.setProperty("webdriver.gecko.driver", Helper.geckoLocation);

        driver = new FirefoxDriver();

        driver.navigate().to(this.URI);
    }

    @Test
    public void demoOpenCartCheckout() throws InterruptedException
    {
        // klik na my account
        WebElement dropdownMenu = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/a"));
        dropdownMenu.click();

        // klik na login
        WebElement loginButton = driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul/li[2]/ul/li[2]/a"));
        loginButton.click();

        // unos email adrese
        WebElement emailTextbox = driver.findElement(By.name("email"));
        emailTextbox.sendKeys(Helper.email);

        // unos lozinke
        WebElement passwordTextbox = driver.findElement(By.name("password"));
        passwordTextbox.sendKeys(Helper.password);

        // odabir tipke za nastavak procesa prijave
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/div/form/input"));
        continueButton.click();

        // odlazak na početnu stranicu
        WebElement homepageRedirect = driver.findElement(By.xpath("/html/body/header/div/div/div[1]/div/h1/a"));
        homepageRedirect.click();
        Thread.sleep(500);

        // odabir padajućeg izbornika za laptope
        WebElement laptopDropdownMenu = driver.findElement(By.xpath("/html/body/div[1]/nav/div[2]/ul/li[2]/a"));
        laptopDropdownMenu.click();

        // odabir opcije za prikaz svih laptopa
        WebElement showAllSelection = driver.findElement(By.xpath("/html/body/div[1]/nav/div[2]/ul/li[2]/div/a"));
        showAllSelection.click();
        Thread.sleep(500);

        // odabir HP laptopa
        WebElement hpSelection = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[4]/div[1]/div/div[2]/div[1]/h4/a"));
        hpSelection.click();
        Thread.sleep(500);

        // odabir datuma dostave
        WebElement deliveryDate = driver.findElement(By.xpath("//*[@id=\"input-option225\"]"));
        deliveryDate.clear();
        deliveryDate.sendKeys("2018-10-23");

        // odabir količine traženog proizvoda
        WebElement quantity = driver.findElement(By.xpath("//*[@id=\"input-quantity\"]"));
        quantity.clear();
        quantity.sendKeys("2");

        // dodavanje proizvoda u košaricu
        WebElement addToCart = driver.findElement(By.xpath("//*[@id=\"button-cart\"]"));
        addToCart.click();
        Thread.sleep(1000);

        // odabir košarice pri vrhu web stranice
        WebElement cart = driver.findElement(By.xpath("/html/body/header/div/div/div[3]/div/button"));
        cart.click();
        Thread.sleep(1000);

        // odabir tipke Checkout
        WebElement checkout = driver.findElement(By.xpath("/html/body/header/div/div/div[3]/div/ul/li[2]/div/p/a[2]"));
        checkout.click();
        Thread.sleep(1500);

        /*
        // unos imena
        WebElement firstnameTextbox = driver.findElement(By.name("firstname"));
        firstnameTextbox.sendKeys("Pero");

        // unos prezimena
        WebElement lastnameTextbox = driver.findElement(By.name("lastname"));
        lastnameTextbox.sendKeys("Peric");

        // unos adrese
        WebElement addressTextbox = driver.findElement(By.name("address_1"));
        addressTextbox.sendKeys("Kneza Trpimira 2B");

        // unos grada
        WebElement cityTextbox = driver.findElement(By.name("city"));
        cityTextbox.sendKeys("Osijek");

        // unos poštanskog broja
        WebElement postalcodeTextbox = driver.findElement(By.name("postcode"));
        postalcodeTextbox.sendKeys("31000");

        // odabir države
        Select countrySelect = new Select(driver.findElement(By.name("country_id")));
        countrySelect.selectByValue("53");
        Thread.sleep(1000);

        // odabir županije
        Select regionSelect = new Select(driver.findElement(By.name("zone_id")));
        regionSelect.selectByValue("856");
        */

        // potvrda platne adrese
        WebElement addressConfirmation = driver.findElement(By.xpath("//*[@id=\"button-payment-address\"]"));
        addressConfirmation.click();
        Thread.sleep(1500);

        // potvrda adrese dostave
        WebElement deliveryDetails = driver.findElement(By.xpath("//*[@id=\"button-shipping-address\"]"));
        deliveryDetails.click();
        Thread.sleep(1500);

        // dodavanje komentara uz narudžbu
        WebElement orderComment = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[4]/div[2]/div/p[4]/textarea"));
        orderComment.clear();
        orderComment.sendKeys("Very good price!");

        // odabir načina dostave
        WebElement deliveryConfirm = driver.findElement(By.xpath("//*[@id=\"button-shipping-method\"]"));
        deliveryConfirm.click();
        Thread.sleep(1500);

        // pristanak na pravila trgovca
        WebElement termsAndConditions = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[5]/div[2]/div/div[2]/div/input[1]"));
        termsAndConditions.click();
        Thread.sleep(1000);

        // odabir metode plaćanja
        WebElement paymentMethodConfirm = driver.findElement(By.xpath("//*[@id=\"button-payment-method\"]"));
        paymentMethodConfirm.click();
        Thread.sleep(4000);

        // potvrda narudžbe
        WebElement confirmOrder = driver.findElement(By.xpath("//*[@id=\"button-confirm\"]"));
        confirmOrder.click();
        Thread.sleep(2000);

        // uzimanje teksta s nove stranice kako bi se odredio uspjeh ili neuspjeh testa
        WebElement orderText = driver.findElement(By.xpath("/html/body/div[2]/div/div/h1"));
        String text = orderText.getText();

        if(text.contains("Your order has been placed!"))
        {
            System.out.println("8. testni slučaj: uspjeh! Uspješna kupnja.");
        }
        else
        {
            System.out.println("8. testni slučaj: neuspjeh! Neuspješna kupnja.");
        }

    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }

}
