
Ovaj projekt izrađen je u IntelliJ IDE 2018.3.3 korištenjem testNG i selenium okvira za automatizaciju testiranja programske podrške. 
Unutar pom.xml dodane su dvije ovisnosti: prva za <a href="https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java/3.14.0">selenium 3.14.0</a>, a druga za <a href="https://mvnrepository.com/artifact/org.testng/testng/6.14.3">testNG 6.14.3</a>. koje su preuzete direktno s maven repozitorija.
WebDriver za Firefox preuzet je s navedene <a href="https://github.com/mozilla/geckodriver/releases">poveznice</a>. Za potrebe izrade projekta korišten je <a href="https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html">Java SDK 11.0.1</a>.

Web sjedište koje se testiralo je dano <a href="https://demo.opencart.com/index.php?route=common/home">poveznicom</a>. Pokušalo se odabrati nekakvo pravo web sjedište poput <a href="https://store.steampowered.com/">Steama</a>, no nažalost ne prihvaćaju "jednokratne" email adrese za sjedišta poput <a href="https://www.mailinator.com">mailinatora</a>.
Također je nemoguće izvršiti automatizaciju procesa registracije i prijave zbog potrebnih Captcha unosa pa sam pretpostavio da većina sjedišta ima tako postavljene mehanizme koji onemogućuju automatizaciju kako bi onemogućili razne vrste napada. Iz tih razloga odabrano je sjedište koje omogućuje jednostavnu automatizaciju.

Testni slučajevi koji su kreirani za ovaj projekt dani su u nastavku:

		1. Registracija korisnika,
		2. Negativni test registracije korisnika,
		3. Prijava korisnika na web sjedište,
		4. Odlogiravanje korisnika,
		5. Oporavak računa zbog zaboravljene lozinke,
		6. Usporedba dva uređaja na listi usporedbe,
		7. Dodavanje dva i uklanjanje jednog od uređaja s liste usporedbe,
		8. Izvršavanje kupnje jednog uređaja s web sjedišta.
		
Komentar za 8. testni slučaj: sadržava komentirani blok koda koji kreira adresu zato što se prilikom pokretanja tog testnog slučaja izvršilo kreiranje adresa te se ta adresa više ne može ukloniti za korišteni korisnički račun. Otkomentirani dio koda pretpostavlja da će se "kupovina" izvršiti s postojećim korisničkim računom i adresom.

Pokušalo se dodati vrijeme pauziranja izvršavanja testa kako bi se uspjelo promijeniti stanje web stranice, no još uvijek u osmom test slučaju s vremena na vrijeme može se dogoditi da test ne prođe (primijetio sam da web stranica nekada ne osvježi sadržaj potreban za potvrdu narudžbe. Metoda se nalazi na 161. liniji u CheckoutTest klasi).
Kreirana je i testNG.xml datoteka u kojoj su opisani svi testni slučajevi zbog jednostavnosti pokretanja testova. Uspjeh ili neuspjeh određenog testnog slučaja određuje se ispitivanjem sadržaja stringa koji se dobije pristupom na određenu poveznicu web stranice te se u konzolu ispiše odgovarajući tekst. U Helper klasi sadržane su konstante koje se koriste u testnim slučajevima.
